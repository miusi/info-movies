package com.miusi.infomovies.data.remote

import com.miusi.infomovies.application.AppConstants
import com.miusi.infomovies.data.model.MovieList
import com.miusi.infomovies.repository.WebService

//Token: 3cb29d41bf3a146b16104795f74e2fd7

class MovieDataSource(private val webService: WebService) {

    suspend fun getUpcomingMovies(): MovieList = webService.getUpcomingMovies(AppConstants.API_KEY)

    suspend fun getTopRatedMovies(): MovieList = webService.getTopRatedMovies(AppConstants.API_KEY)

    suspend fun getPopularMovies(): MovieList = webService.getPopularMovies(AppConstants.API_KEY)

}