package com.miusi.infomovies.ui.moviedetails

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.miusi.infomovies.R
import com.miusi.infomovies.databinding.FragmentMovieDetailBinding


class MovieDetailFragment : Fragment(R.layout.fragment_movie_detail) {

    private lateinit var mBinding: FragmentMovieDetailBinding
    private val args by navArgs<MovieDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = FragmentMovieDetailBinding.bind(view)

        Glide.with(requireContext())
            .load("https://image.tmdb.org/t/p/w500/${args.posterImageUrl}")
            .centerCrop()
            .into(mBinding.imgMovie)

        Glide.with(requireContext())
            .load("https://image.tmdb.org/t/p/w500/${args.backgroundImageUrl}")
            .centerCrop()
            .into(mBinding.imgBackground)

        mBinding.tvDescription.text = args.overview
        mBinding.tvTitle.text = args.title
        mBinding.textLanguage.text = "Language ${args.language}"
        mBinding.textRating.text = "${args.voteAverage} (${args.voteCount} Reviews)"
        mBinding.textReleased.text = "Released ${args.releaseDate}"

    }

}