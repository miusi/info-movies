# Info Movies

## Descripción
App creada para visualizar películas por diferentes categorias.

## Features
- Arquitectura MVVM
- Fragments
- Uso de LiveData
- Navigation con SafeArgs
- Retrofit
- Concat Adapter
